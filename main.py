import sys
import model, view, controller

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication,QGridLayout, QMainWindow, QPushButton, QTabWidget, QWidget, QTableView, QTableWidget

if __name__ == "__main__":
    app = QApplication(sys.argv)

    mdl = model.Model()
    mw = view.UI_MainWindow(mdl)
    ctr = controller.Controller(mw, mdl)

    mw.show()
    sys.exit(app.exec_())