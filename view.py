import sys, os

import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigC

from ctypes import windll
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QGridLayout, QVBoxLayout, QMainWindow, QPushButton, QTabWidget, QWidget, QTableView, QTableWidget, QLayout
# from PyQt5.QtCore import *
# from PyQt5.QtGui import *

class UI_MainWindow(QMainWindow):
    def __init__(self, mdl, parent=None):
        super(UI_MainWindow, self).__init__(parent)
        self.mdl = mdl

    def reg_ctr(self,ctr): #コントローラをバインド
        self.ctr = ctr
        self.initUI()
        # self.langUI()

    def initUI(self): #UIを初期化
        self.setWindowTitle('FLIPR Data analyzer')
        disp_w = windll.user32.GetSystemMetrics(0)
        disp_h = windll.user32.GetSystemMetrics(1)
        self.setGeometry(disp_w/10,disp_h/10,disp_w*8/10,disp_h*8/10)
        self.l_base = QGridLayout()
        self.w = QWidget(self)

        lb = self.l_base
        ### ウィジェット配置ここから ###

        self.lu_tw = QTabWidget(self)
        self.lu_tw.addTab(QWidget(),"lu")
        lb.addWidget(self.lu_tw,0,0,3,4)
        
        self.ru_tw = QTabWidget(self)
        self.ru_tw.addTab(QWidget(),"ru")
        lb.addWidget(self.ru_tw,0,4,3,2)
      
        self.bt_tw = QTabWidget(self)
        self.bt_tw.addTab(QWidget(),"bt")
        lb.addWidget(self.bt_tw,3,0,2,6)


        self.lu_tw.addTab(tabw_96plot(),"test")

        self.w.setLayout(self.l_base)
        self.setCentralWidget(self.w)

class tabw_96plot(QWidget):
    def __init__(self, parent=None):
        super(tabw_96plot, self).__init__(parent)
        self.initUI()

    def initUI(self):
        self.l_plot =QGridLayout(self)
        self.l_plot.setHorizontalSpacing(0)
        self.l_plot.setVerticalSpacing(0)

        self.initFig(self.l_plot)


    def initFig(self,l):
        list_fc =[]
        for y in range(2):
            list_fc_x = []
            for x in range(3):
                fcw = Qwid_FigC(self)
                l.addWidget(fcw,y,x)
                

                fcw.fc.axes = fcw.fig.add_subplot(111)
                fcw.fc.axes.plot([x,x*2,x*3],[y,y*2,y*3])

                

class Qwid_FigC(QWidget):
    def __init__(self, parent=None):
        super(Qwid_FigC,self).__init__(parent)
        self.initUI()
    
    def initUI(self):
        self.l = QVBoxLayout(self)
        self.fig = plt.figure()
        self.fc = FigC(self.fig)
        self.l.addWidget(self.fc)
        self.fc.setStyleSheet("""
                background-color: red;
        """)
        # print(self.fc)
        # print(self.fig)




    